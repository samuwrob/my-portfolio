from django.urls import path
from . import views


urlpatterns = [
    path('home', views.home, name='home'),
    path('', views.header, name='header'),
    path('test', views.test, name="test"),
    path('projects', views.projects, name="projects"),
]