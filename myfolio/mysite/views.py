from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home.html', {})
def test(request):
    return render(request, 'test.html', {})
    
def projects(request):
    return render(request, 'projects.html', {})
def header(request):
    return render(request, 'header.html', {})